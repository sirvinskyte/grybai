﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grybai
{
    public partial class Form1 : Form
    {
        const string failas = "grybai.txt";

        public Form1()
        {
            InitializeComponent();
        }

        private void Issaugoti_Click(object sender, EventArgs e)
        {
            try
            {
                int menuo = Convert.ToInt32(Menuo.Text);
                int diena = Convert.ToInt32(Diena.Text);
                double mase = Convert.ToDouble(Mase.Text);
                int raudonvirsiai = Convert.ToInt32(Raudonvirsiai.Text);
                int baravykai = Convert.ToInt32(Baravykai.Text);

                File.AppendAllText(failas, menuo + " " + 
                                           diena + " " +
                                           mase + " " + 
                                           raudonvirsiai + " " +
                                           baravykai);
                File.AppendAllText(failas, Environment.NewLine);
            }
            catch
            {
                MessageBox.Show("Neteisingai pateikti duomenys");
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] lines = File.ReadAllLines(failas);


            int[] menesiai = new int[lines.Length];
            int[] dienos = new int[lines.Length];
            double[] mases = new double[lines.Length];
            int[] raudonvirsiaivisi = new int[lines.Length];
            int[] baravykaivisi = new int[lines.Length];

            for (int i = 0; i < lines.Length; i++)
            {
                string[] iteams = lines[i].Split(' ');

                menesiai[i] = Convert.ToInt32(iteams[0]);
                dienos[i] = Convert.ToInt32(iteams[1]);
                mases[i] = Convert.ToDouble(iteams[2]);
                raudonvirsiaivisi[i] = Convert.ToInt32(iteams[3]);
                baravykaivisi[i] = Convert.ToInt32(iteams[4]);
            }
            double didziausias = mases[0];
            int didziausioindeksas = 0;
            for (int i = 0; i < mases.Length; i++)
            {
                if(mases[i] > mases[0])
                {
                    didziausias = mases[i];
                    didziausioindeksas = i;
                }
            }

            textBox1.Text = Convert.ToString(menesiai[didziausioindeksas]);
            textBox2.Text = Convert.ToString(dienos[didziausioindeksas]);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string[] lines = File.ReadAllLines(failas);


            int[] menesiai = new int[lines.Length];
            int[] dienos = new int[lines.Length];
            double[] mases = new double[lines.Length];
            int[] raudonvirsiaivisi = new int[lines.Length];
            int[] baravykaivisi = new int[lines.Length];

            for (int i = 0; i < lines.Length; i++)
            {
                string[] iteams = lines[i].Split(' ');

                menesiai[i] = Convert.ToInt32(iteams[0]);
                dienos[i] = Convert.ToInt32(iteams[1]);
                mases[i] = Convert.ToDouble(iteams[2]);
                raudonvirsiaivisi[i] = Convert.ToInt32(iteams[3]);
                baravykaivisi[i] = Convert.ToInt32(iteams[4]);
            }
            double didziausias = baravykaivisi[0];
            int didziausioindeksas = 0;
            for (int i = 0; i < baravykaivisi.Length; i++)
            {
                if (baravykaivisi[i] > baravykaivisi[0])
                {
                    didziausias = baravykaivisi[i];
                    didziausioindeksas = i;
                }
            }

            textBox1.Text = Convert.ToString(menesiai[didziausioindeksas]);
            textBox2.Text = Convert.ToString(dienos[didziausioindeksas]);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string[] lines = File.ReadAllLines(failas);


            int[] menesiai = new int[lines.Length];
            int[] dienos = new int[lines.Length];
            double[] mases = new double[lines.Length];
            int[] raudonvirsiaivisi = new int[lines.Length];
            int[] baravykaivisi = new int[lines.Length];

            for (int i = 0; i < lines.Length; i++)
            {
                string[] iteams = lines[i].Split(' ');

                menesiai[i] = Convert.ToInt32(iteams[0]);
                dienos[i] = Convert.ToInt32(iteams[1]);
                mases[i] = Convert.ToDouble(iteams[2]);
                raudonvirsiaivisi[i] = Convert.ToInt32(iteams[3]);
                baravykaivisi[i] = Convert.ToInt32(iteams[4]);
            }
            double didziausias = raudonvirsiaivisi[0];
            int didziausioindeksas = 0;
            for (int i = 0; i < raudonvirsiaivisi.Length; i++)
            {
                if (raudonvirsiaivisi[i] > raudonvirsiaivisi[0])
                {
                    didziausias = raudonvirsiaivisi[i];
                    didziausioindeksas = i;
                }
            }

            textBox1.Text = Convert.ToString(menesiai[didziausioindeksas]);
            textBox2.Text = Convert.ToString(dienos[didziausioindeksas]);
        }
    }
}
